#include "Card.h"
#include "Deck.h"
#include "Market.h"
#include "Player.h"
#include "Human.h"
#include "AllBonus.h"
#include "AllTokens.h"
#include "Game.h"
#include <iostream>
#include <string>

using std::string;
using std::vector;
using std::cout;
using std::endl;
using std::cin;


int main(){
    // initialize deck and market
    Deck   d1;
    d1.shuffle();
    Market m1(d1);

    Human p1(d1,"name1");
    Human p2(d1,"name2");

    Game g1(p1,p2);

    g1.getTokens().printTokens();

    // initialize player
   /* int mode;
    string n1,n2;

    cout << "Game starts! Choose Modes---> 1: AI vs. AI; 2: Human vs. Human; 3: Human vs. AI." << endl; 
    cin >> mode;

    switch(mode){
        case 1:
        {
            cout << " AI vs. AI " << endl;     
            break;
        }
        case 2:
        {
            cout << "Enter Name for Player 1: ";
            cin >> n1;
            cout << "Enter Name for Player 2: ";
            cin >> n2;
            p1.setName(n1);
            p2.setName(n2);
            break;
        }
        case 3:
        {
            cout << "Enter Name for Human Player: ";
            cin >> n1;
            p1.setName(n1); 
            break;
        }
    }
*/
    
    return 0;
}

