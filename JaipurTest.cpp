#include "Jaipur.h"
#include "Token.h"
#include "AllTokens.h"
#include "Test_Jaipur.h"
#include "Card.h"
#include "Deck.h"
#include "Market.h"
#include "AllBonus.h"
#include <string>
#include <iostream>
#include <vector>
#include <cassert>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;

int main(){
	 /*==============================test for Market============================*/
  //test default constructor
    Market mymarket;
    test_market(mymarket);
   
    //test consturctor making up of three camels and two cards from deck
    Deck tdeck;
    tdeck.shuffle();
    Market mymarket2(tdeck);
    test_market32(mymarket2);

    // test getGoods which return a cards that specified by an integer in the market
    test_getGoods(mymarket2);

    //test setGoods1 that fill the market with cards from deck
    Market mymarket3(tdeck); //after being drawed two crads just now ,th top cards on the deck is leather, camel, camel, silver
    mymarket.getGoods(1);// draw  a camel
    mymarket.setGoods(tdeck);// fill a leather
    mymarket.getGoods(1);//draw a camel 
    mymarket.setGoods(tdeck);//fill a camel 
    mymarket.getGoods(1);//draw a camel 
    mymarket.setGoods(tdeck);// fill a camel
    test_setGoods1(mymarket3); // mymarket3 now is spice spice leather camel camel

    // test delGoods and getSize
    Market mymarket4;
    test_delget(mymarket4);


    /*==============================test for Decks============================*/
    //test default deck constructor
    Deck mydeck;
    test_Deck(mydeck);

    // test shuffle function
    Deck mydeck2;
    mydeck2.shuffle();
    test_shuffle(mydeck2);

    Token t1;
    /*==============================test for AllTokens============================*/
    // test the default constructor
    All_Tokens at1;
    test_AllTokens(at1);
    // test for getSize()
    All_Tokens at2;
    test_ATgetSize(at2);

    // test for getToken()
    All_Tokens at3;
    test_ATgetToken(at3);

    // test printTokens
    All_Tokens at4;
    for (int m = 0; m < 5; ++m)
        at4.deleteToken(2);
    at4.printTokens();

/*==============================test for AllBonus============================*/

  //test default constructor
  AllBonus abtest1;
  test_AllBonus(abtest1);
 
  //test getSize()
  AllBonus abtest2;
  test_ABgetSize(abtest2);

  //test getBonus()
  AllBonus abtest3;
  test_ABgetBonus(abtest3);

  //test shuffle()
  AllBonus abtest4;
  abtest4.shuffle();
  test_ABshuffle(abtest4);

  //test pBonus
  AllBonus abtest5;
  abtest5.pBonus();
  abtest5.delBonus(3);
  abtest5.delBonus(3);
  abtest5.delBonus(4);
  abtest5.delBonus(5);

  abtest5.pBonus();
  abtest5.delBonus(3);
  abtest5.delBonus(3);
  abtest5.delBonus(3);
  abtest5.delBonus(3);

  abtest5.pBonus();
  cout << " test for All_Bonuns: pBonus() is passed"<<endl;

/*==============================test for Player============================*/
    Deck D_p;
    D_p.shuffle();

    All_Tokens at5;
    All_Bonus ab6;

    Human p1(D_p, "test");
    vector<string> Init_Cards; 
    Init_Cards.push_back("SPICE"); 
    Init_Cards.push_back("SPICE");
    Init_Cards.push_back("LEATH");
    Init_Cards.push_back("CAMEL");
    Init_Cards.push_back("CAMEL");

    Market m1(D_p);

    // test for constructor, getHand, getPoints
    test_player_cons(p1, Init_Cards);

    // test for sell

    vector<int> sells = {1,2}; // give the location of cards to sell, from 1
    test_player_sell(sells, at5, ab6, p1);

    //test for take
    test_player_take(p1, m1, D_p);

    //m1.printMarket();

    // test for trade
    vector<int> mc = {5,4}; // location from 1
    vector<int> hc = {2}; // give the location of cards to trade, from 1
    test_player_trade(mc, hc, m1, p1);

    // test for takeCamel
    test_player_takeCamel(m1,D_p, p1);

    // test for printPlayer
    p1.printPlayer();
    
    cout << "Test for Human, Player passed!" << endl;

    return 0;
}

/*==============================test for Decks============================*/
void test_Deck(Deck &Tdeck){
  string Ctype[7] = {"DIAMD","GOLD","SILVR","CLOTH","SPICE","LEATH","CAMEL"};
  int cnum[7]={6,6,6,8,8,10,11};
  int i,j;

  for(i = 6; i > 0; i--){
    for(j = 0; j < cnum[i]; j++){
      assert(Ctype[i] == Tdeck.drawCard().getCard());
    }
  }
  cout << "test for Deck: default constructor have passed" <<endl;
}

void test_shuffle(Deck &Tdeck){
  int Scard[55] = {2,1,5,3,1,1,1,5,2,4,4,7,3,7,6,5,5,6,7,4,7,1,3,4,6,2,6,6,6,7,2,7,6,4,3,6,2,2,7,7,1,4,6,7,5,5,5,3,5,3,7,7,6,4,4}; //the array is the order of the deck after shuffle, the card type is represented by number. To be specifically, '1'-DIAMD,'2'-GOLD,'3'-SILVR,'4'-SPICE,'5'-CLOTH,'6'-LEATH, '7'-CAMEL
  int i ;
  for(i = 54; i >= 0; i++){
    switch (Scard[i]){
          case '1': 
	    assert(Tdeck.drawCard().getCard()=="DIAMD");
          case '2':
	    assert(Tdeck.drawCard().getCard()=="GOLD");
          case '3':
	    assert(Tdeck.drawCard().getCard()=="SILVR");
          case '4':
	    assert(Tdeck.drawCard().getCard()=="SPICE");
          case '5':
	    assert(Tdeck.drawCard().getCard()=="CLOTH");
          case '6':
	    assert(Tdeck.drawCard().getCard()=="LEATH");
	  case '7':
	    assert(Tdeck.drawCard().getCard()=="CAMEL");
    }
  }
  cout << " test for Deck : shuffle() is passsed."<<endl;
}

/*==============================test for AllTokens============================*/
void test_AllTokens(All_Tokens &AT){
    int size[7] = {5,5,5,9,7,7,1};
    string type[7] = {"DIAMD", "GOLD", "SILVR", "LEATH", "SPICE", "CLOTH", "CAMEL"};
    vector<vector<int> > val = {
        {7, 7, 5, 5, 5}, // diamond tokens    
        {6, 6, 5, 5, 5}, // gold tokens
        {5, 5, 5, 5, 5}, // silver tokens
        {4, 3, 2, 1, 1, 1, 1, 1, 1}, // leather tokens
        {5, 3, 3, 2, 2, 1, 1}, // spice tokens
        {5, 3, 3, 2, 2, 1, 1}, //cloth tokens
        {5} // camel tokens
    };
    int i,j;

    for (i = 0; i < 7; ++i){
        for (j = 0; j < size[i]; ++j){
            assert(AT.getToken(i+1).getType() == type[i]);
            assert(AT.getToken(i+1).getVal() == val.at(i).at(j));
            AT.deleteToken(i+1);
        }
    }

    cout << "Test for All_Tokens default constructor passed!" << endl;
}

void test_ATgetSize(All_Tokens &AT){
    int size[7] = {5,5,5,9,7,7,1};
    int i;
    for (i = 0; i < 7; ++i){
        assert(AT.getSize(i+1) == size[i]); 
        AT.deleteToken(i+1);
        size[i]--;
    }
    cout << "Test for All_Tokens: getSize() passed!" << endl;
}

void test_ATgetToken(All_Tokens &AT){
    string type[7] = {"DIAMD", "GOLD", "SILVR", "LEATH", "SPICE", "CLOTH", "CAMEL"};
    vector<vector<int> > val = {
        {7, 7, 5, 5, 5}, // diamond tokens    
        {6, 6, 5, 5, 5}, // gold tokens
        {5, 5, 5, 5, 5}, // silver tokens
        {4, 3, 2, 1, 1, 1, 1, 1, 1}, // leather tokens
        {5, 3, 3, 2, 2, 1, 1}, // spice tokens
        {5, 3, 3, 2, 2, 1, 1}, //cloth tokens
        {5} // camel tokens
    };
    int i,j;

    for (i = 0; i < 7; ++i){
        for (j = 0; j < AT.getSize(i+1); ++j){
            assert(AT.getToken(i+1).getType() == type[i]);
            assert(AT.getToken(i+1).getVal() == val.at(i).at(j));
            AT.deleteToken(i+1);
        }
    }
    cout << "Test for All_Tokens: getToken() passed!" << endl;
}

// deleteToken has been already tested in the testing of getSize, getToken and default constructor

/*==============================test for AllBonus============================*/

 void test_AllBonus(All_Bonus &AB){
    int size[3]={6,6,6};
    string type[3]={"3Bonus","4Bonus","5Bonus"};
    vector<vector<int>> val = {
      {3,3,2,2,1,1},
      {6,6,5,5,4,4},
      {10,10,9,9,8,8}
    };

    int i,j;
    for (i = 0; i < 3; ++i){
      for (j = 0; j < size[i]; ++j){
        	assert(AB.getBonus(i+3).getType()==type[i]);
        	assert(AB.getBonus(i+3).getVal()==val.at(i).at(j));
        	AB.delBonus(i+3);
      }
    }
    cout <<"test for AllBonus default constructor is passed" <<endl;
 }

void test_ABgetSize(All_Bonus &AB){
  int size[3]={6,6,6};
  int i;
  for(i = 0; i < 3; ++i){
           assert(AB.getSize(i+3)==size[i]);
	   AB.delBonus(i+3);
	   size[i]--;
  }
  cout << "Test for All_Bonus:getSize() passed."<<endl;
}

void test_ABgetBonus(All_Bonus &AB){
    string type[3]={"3Bonus","4Bonus","5Bonus"};
    vector<vector<int>> val = {
      {3,3,2,2,1,1},
      {6,6,5,5,4,4},
      {10,10,9,9,8,8}
    };

    int i,j;

    for(i = 0; i < 3; ++i){
      for(j = 0; j < AB.getSize(i+3); ++j){
        assert(AB.getBonus(i+3).getType() == type[i]);
        assert(AB.getBonus(i+3).getVal()==val.at(i).at(j));
        AB.delBonus(i+3);
      }
    }
    cout<<"Test for All_Bonus:getBonus() passed"<<endl;
}

void test_ABshuffle(All_Bonus &AB){
    string type[3]={"3Bonus","4Bonus","5Bonus"};
    vector<vector<int>> val = {
      {3,2,3,1,2,1},
      {6,5,6,4,5,4},
      {10,9,10,8,9,8}
    };

    int i,j;

    for(i = 0; i < 3; ++i){
      for(j = 0; j < AB.getSize(i+3); ++j){
        assert(AB.getBonus(i+3).getVal()==val.at(i).at(j));
        assert(AB.getBonus(i+3).getType()==type[i]);
        AB.delBonus(i+3);
      }
    }
    cout<<"Test for All_Bonus:shuffle() passed"<<endl;
}

/*==============================test for Player============================*/

void test_player_cons(Human &p, vector<string> &hcards){
    int i;
    int s1,s2;
    // for constructor
    s1 = s2 = 0;
    for (i = 0; i < 5; ++i){
        if(hcards[i] == "CAMEL") 
            s2++;
        else 
            s1++;
    }

    assert(s1 == p.getSize(0));
    assert(s2 == p.getSize(1));

    for (i = 0; i < 3; ++i)
        assert(p.getHand(0,i).getCard() == hcards[i]);

    for (i = 0; i < 2; ++i)
        assert(p.getHand(1,i).getCard() == hcards[i+3]);

    for (i = 0; i < 4; ++i)
        assert(p.getPoints(i) == 0);

    assert(p.getGToken() == 0);
    assert(p.getBToken() == 0);

    // for getHand
    assert(p.getHand(0,0).getCard() == "SPICE");
    assert(p.getHand(0,1).getCard() == "SPICE");
    assert(p.getHand(0,2).getCard() == "LEATH");
    assert(p.getHand(1,0).getCard() == "CAMEL");
    assert(p.getHand(1,1).getCard() == "CAMEL");

    // for getPoints
    assert(p.getPoints(0) == 0);
    assert(p.getPoints(1) == 0);
    assert(p.getPoints(2) == 0);
    assert(p.getPoints(3) == 0);

}

//test for sell
void test_player_sell(vector<int> sells, All_Tokens &AT, All_Bonus &AB, Human &p){
    p.Sell(sells, AT, AB); 

    assert(p.getPoints(0) == 8);
    assert(p.getPoints(1) == 0);
    assert(p.getPoints(2) == 8);
    assert(p.getPoints(3) == 0);

    assert(p.getSize(0) == 1);
    assert(p.getHand(0,0).getCard() == "LEATH");
    assert(p.getSize(1) == 2);

    assert(p.getGToken() == 2);
    assert(p.getBToken() == 0);

    assert(AT.getSize(5) == 5); 
}

// test for take
void test_player_take(Human &p, Market &m, Deck &d){ 
    p.Take(5, m, d);

    assert(p.getSize(0) == 2);
    assert(p.getSize(1) == 2);

    assert(p.getHand(0,1).getCard() == "CLOTH");
    assert(p.getHand(0,0).getCard() == "LEATH");

    assert(m.getSize() == 5);
    assert(m.getGoods(4).getCard() == "SILVR");
    assert(m.getGoods(5).getCard() == "SILVR");
}

// test for trade
void test_player_trade(vector<int> &mc, vector<int> &hc, Market &M, Human &p){
    p.Trade(mc, hc, M);

    assert(M.getGoods(4).getCard() == "CLOTH");
    assert(M.getGoods(5).getCard() == "CAMEL");

    assert(p.getHand(0,0).getCard() == "LEATH");
    assert(p.getHand(0,1).getCard() == "SILVR");
    assert(p.getHand(0,2).getCard() == "SILVR");

    assert(p.getSize(1) == 1);
}

// test for takeCamel
void test_player_takeCamel(Market &M, Deck &D, Human &p){
    p.TakeCamel(M,D);

    assert(p.getSize(1) == 5);
    assert(p.getSize(0) == 3);

    assert(M.getGoods(1).getCard() == "CLOTH");
    assert(M.getGoods(2).getCard() == "CLOTH");
    assert(M.getGoods(3).getCard() == "CLOTH");
    assert(M.getGoods(4).getCard() == "CLOTH");
    assert(M.getGoods(5).getCard() == "CAMEL");
}

    /*==============================test for Market============================*/

void test_market(Market& mymarket){
  Card tempc;
  int i;
  tempc.setCard("CAMEL");

  for(i = 0; i < 5; i++){
    assert(tempc == mymarket.getGoods(i+1).getCard());
  }
}

void test_market32(Market& mymarket){
  Card tempC;
  int i;
  tempC.setCard("CAMEL");

  for(i = 0; i < 3; i++){
    assert(tempC == mymarket.getGoods(i+1).getCard());
  }

  tempC.setCard("SPICE");//the top two cards of the deck are both Spice
  for(i = 3; i < 5; i++){
    assert(tempC== mymarket.getGoods(i+1).getCard());
  }
  cout<<"Market: constructor and get passed" <<endl;
}

void test_getGoods(Market &mymarket){
  Card tempC1;
  Card tempC2;
  tempC1.setCard("CAMEL");
  tempC2.setCard("SPICE");

  for(int i = 4; i >=3 ; i--){ // the fourth and fifth cards would be first top two cards of the deck which are both spices
    assert(tempC2 == mymarket.getGoods(i+1).getCard());
  }

  for(int i =2; i >= 0; i--){ // the first there cards in the market would be camels
    assert(tempC1 == mymarket.getGoods(i+1).getCard());
  }
  cout<<"Market: getGoods and get passed" <<endl;
}
    
void test_setGoods1(Market& mymarket){
  Card tCard1;
  Card tCard2;
  Card tCard3;

  tCard1.setCard("SPICE");
  tCard2.setCard("LEATH");
  tCard3.setCard("CAMEL");

  assert(mymarket.getGoods(5)== tCard3);
  cout<<"Market: setGoods and get passed" <<endl;
}

void test_delget(Market& mymarket)
{
  assert(mymarket.getSize() == 5);
  mymarket.delGoods(3);
  assert(mymarket.getSize() ==4);
  mymarket.delGoods(1);
  assert(mymarket.getSize() == 3);
  cout<<"Market: getSize() and DelGoods get passed" <<endl;
}

