C = g++
#CFLAGS = -std=c++11 -g -pedantic -Wall -Wextra -O0 -fprofile-arcs -ftest-coverage
CFLAGS = -std=c++11 -g -pedantic -Wall -Wextra -O

#compilation of test 
Jaipur: Jaipur.o Token.o AllTokens.o AllBonus.o Deck.o Card.o Market.o Player.o Human.o
	 $(CC) $(CFLAGS) -o Jaipur Jaipur.o Token.o AllTokens.o AllBonus.o Deck.o Card.o Market.o Player.o Human.o

#compilation of test 
test: JaipurTest.o Token.o AllTokens.o AllBonus.o Deck.o Card.o Market.o Player.o Human.o
	 $(CC) $(CFLAGS) -o JaipurTest JaipurTest.o Token.o AllTokens.o AllBonus.o Deck.o Card.o Market.o Player.o Human.o

%.o: %.cpp 
	 $(CC) $(CFLAGS) -c $*.cpp

clean:
	 \rm *.o test
