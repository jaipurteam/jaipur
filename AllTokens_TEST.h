#ifndef _DECK_TEST_H
#define _DECK_TEST_H


void Deck_TEST::runAllTests(){
	 this->test_AllTokens();
	 this->ATgetSize();
	 this->ATgetToken();
}


void Deck_TEST::test_AllTokens(){
	 int size[7] = {5,5,5,9,7,7,1};
    string type[7] = {"DIAMD", "GOLD", "SILVR", "LEATH", "SPICE", "CLOTH", "CAMEL"};
    vector<vector<int> > val = {
				{7, 7, 5, 5, 5}, // diamond tokens    
				{6, 6, 5, 5, 5}, // gold tokens
				{5, 5, 5, 5, 5}, // silver tokens
				{4, 3, 2, 1, 1, 1, 1, 1, 1}, // leather tokens
				{5, 3, 3, 2, 2, 1, 1}, // spice tokens
				{5, 3, 3, 2, 2, 1, 1}, //cloth tokens
				{5} // camel tokens
	 };
	 int i,j;
 
	 for (i = 0; i < 7; ++i){
		  for (j = 0; j < size[i]; ++j){
				assert(AT.getToken(i+1).getType() == type[i]);
				assert(AT.getToken(i+1).getVal() == val.at(i).at(j));
				AT.deleteToken(i+1);
		  }
	 }
 
	 cout << "Test for All_Tokens default constructor passed!" << endl;
	 }
	  
void Deck_TEST::test_ATgetSize(All_Tokens &AT){
		  int size[7] = {5,5,5,9,7,7,1};
		  int i;
		  for (i = 0; i < 7; ++i){
				assert(AT.getSize(i+1) == size[i]);
				AT.deleteToken(i+1);
				size[i]--;
		  }
		  cout << "Test for All_Tokens: getSize() passed!" << endl;
	 }

void Deck_TEST::test_ATgetToken(All_Tokens &AT){
	 string type[7] = {"DIAMD", "GOLD", "SILVR", "LEATH", "SPICE", "CLOTH", "CAMEL"};
	 vector<vector<int> > val = {
		  {7, 7, 5, 5, 5}, // diamond tokens    
		  {6, 6, 5, 5, 5}, // gold tokens
		  {5, 5, 5, 5, 5}, // silver tokens
		  {4, 3, 2, 1, 1, 1, 1, 1, 1}, // leather tokens
		  {5, 3, 3, 2, 2, 1, 1}, // spice tokens
		  {5, 3, 3, 2, 2, 1, 1}, //cloth tokens
		  {5} // camel tokens
	 };
	 int i,j;

	 for (i = 0; i < 7; ++i){
		  for (j = 0; j < AT.getSize(i+1); ++j){
				assert(AT.getToken(i+1).getType() == type[i]);
				assert(AT.getToken(i+1).getVal() == val.at(i).at(j));
				AT.deleteToken(i+1);
		  }
	 }
		  cout << "Test for All_Tokens: getToken() passed!" << endl;
	 }

void Deck_TEST::test_deleteToken(int tokensDel[5]){
	 for(int i = 0; i < tokensDel.size(); i++){
		  this->deleteToken(2);
		  this->deleteToken(2);
		  this->deleteToken(2);
		  this->deleteToken(2);
	 }
	 this->printTokens();
}
#endif
