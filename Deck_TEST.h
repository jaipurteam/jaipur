/*==============================test for Decks============================*/

ifndef _DECK_TEST_H
#define _DECK_TEST_H


void Deck_TEST::runAllTests(){
	 test_Deck();
	 test_shuffle();
}

void Deck_TEST::test_Deck(){
  string Ctype[7] = {"DIAMD","GOLD","SILVR","CLOTH","SPICE","LEATH","CAMEL"};
  int cnum[7]={6,6,6,8,8,10,11};
  int i,j;

  for(i = 6; i > 0; i--){
    for(j = 0; j < cnum[i]; j++){
      assert(Ctype[i] == this->drawCard().getCard());
    }   
  }
  cout << "test for Deck: default constructor have passed" <<endl;
}

void Deck_TEST::test_shuffle(){
  int Scard[55] = {2,1,5,3,1,1,1,5,2,4,4,7,3,7,6,5,5,6,7,4,7,1,3,4,6,2,6,6,6,7,2,7,6,4,3,6,2,2,7,7,1,4,6,7,5,5,5,3,5,3,7,7,6,4,4}; //the array is the order of the deck after shuffle, the card type is represented by number. To be specifically, '1'-DIAMD,'2'-GOLD,'3'-SILVR,'4'-SPICE,'5'-CLOTH,'6'-LEATH, '7'-CAMEL
  int i ; 
  for(i = 54; i >= 0; i++){
    switch (Scard[i]){
          case '1': 
       assert(this->drawCard().getCard()=="DIAMD");
          case '2':
       assert(this->drawCard().getCard()=="GOLD");
          case '3':
       assert(this->drawCard().getCard()=="SILVR");
          case '4':
       assert(this->drawCard().getCard()=="SPICE");
          case '5':
       assert(this->drawCard().getCard()=="CLOTH");
          case '6':
       assert(this->drawCard().getCard()=="LEATH");
     case '7':
       assert(this->drawCard().getCard()=="CAMEL");
    }   
  }
  cout << " test for Deck : shuffle() is passsed."<<endl;
}
#endif
