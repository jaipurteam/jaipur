#include "AI.h"
#include <string>
#include <iostream>

using std::string;
using std::vector;
using std::cout;
using std::endl;

AI::AI(Deck &deck, string AIname):Player(deck, AIname){}

void AI::setAction(){
  /*    int tempM[5];//an array that store the information of market,the integer in the array represent the five cards in the market.ie. first number tempM[0] represent first card in market, the value varies from 1-7,they correspond with "DIAMD","GOLD","SILVR","CLOTH","SPICE","LEATH","CAMEL" respectively. 
    int tempT[9];//an array that store the number of 9 kinds of Tokens left on the desk. from tempT[0] to tempT[8], they store the number of "DIAMD","GOLD","SILVR","CLOTH","SPICE","LEATH","CAMEL","3Bonus""4Bonus","5Bonus"respectively.
    int tempH[7];//an array that store the integer that represent the type of goods in the hand, the value in the array range from 1-7, they correspond with "DIAMD","GOLD","SILVR","CLOTH","SPICE","LEATH","CAMEL" respectively.
  */
}

void AI::TransCard(std::vector<std::string>& types,std::vector<int> &NumedType){

  unsigned int i;

  for(i = 0; i < types.size(); i++){

    if (types.at(i) == "DIAMD" ){
      NumedType.push_back(1);
    }
    else if (types.at(i) == "GOLD" ){
      NumedType.push_back(2);
    }
    else if (types.at(i) == "SILVR" ){
      NumedType.push_back(3);
    }
    else if (types.at(i) == "SPICE" ){
      NumedType.push_back(4);
    }
    else if (types.at(i) == "CLOTH" ){
      NumedType.push_back(5);
    }
    else if (types.at(i) == "LEATH" ){
      NumedType.push_back(6);
    }
    else if (types.at(i) == "CAMEL" ){
      NumedType.push_back(7);
    }
    else{ 
      cout <<"Illegal card type"<<endl;
    }
  }
}

void AI::BuildToken(All_Tokens &mytoken, All_Bonus &mybonus, std::vector<std::vector<int>>& AllTokenNum){

  std::vector<int> TokenNum;
  std::vector<int> BonusNum;
  int i,j;

  for( i = 1; i <= 7;i++){
    TokenNum.push_back(mytoken.getSize(i));
  }

  for( j = 3; j <= 5; j++){
    BonusNum.push_back(mybonus.getSize(j));
  }

  AllTokenNum.push_back(TokenNum);
  AllTokenNum.push_back(BonusNum);
}

