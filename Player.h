#ifndef PLAYER_H
#define PLAYER_H

#include "Card.h"
#include "AllBonus.h"
#include "AllTokens.h"
#include "Market.h"
#include <string>
#include <iostream>
#include <vector>

class Player{ //this is an abstract class bc setAction is a pure virtural function
 private: 
  std::vector<std::vector<Card>> hand;// good cards, herd 
  std::string name;
  int size[2]; // number of cards and camels
  int point[4];// basic, bonus, total and seal; 
 // int Action;// sell, take or trade (player's or ai's choice of action)
  int GTokenNum;// # of goods token earned
  int BTokenNum; // # of bonus token earned

 protected:
  virtual bool setAction()=0;// pure virtual function that will be defined in the AI and Human 

 public:
  Player();
  Player(Deck& , std::string);//initialize a player
  virtual ~Player();
  //void setName(std::string );// set up player's name
  //void setPoints(int );//update points
  //void setTokenNum(); // update the token numbers
  void delHand(int, int ); // delete the card from player's hand that has been played 
  void setHand(Card);//update hands, if 'CAMEL' set it into herds, otherwise into cards
  //void sortHand();//sorts hand by type
  int getPoints(int) const;// get points of a player, integer is the index of the 4 types of points ie(basic, bonus, total, seal) 
  int getSize(int) const;
  int getGToken() const {return GTokenNum;};
  int getBToken() const {return BTokenNum;};
  void Sell(std::vector<int>, All_Tokens &, All_Bonus &);//sell the cards included in vector<int> by location
  //firs two vector<int>: cards need to exchange in market and hand card;
  //take several goods from market and exchange the same # of cards from hand
  //# of camel need to exchange = 1st int - 2nd int
  void Trade(std::vector<int>, std::vector<int>, Market &);
  void Take(int, Market &, Deck &);//int means the location on market, take only 1 card from market, and draw a card from deck to market
  void TakeCamel(Market &, Deck &); // take all the camels on market into herds
  Card getHand(int, int) const ;//1st int indicates card or herds: 0--card, 1--herds; 2nd int means which handcard in card or herds 
  std::string getName() const ;// get the name of a player
  void printPlayer();//hands, points,tokens
  void setName(std::string);
};

#endif
