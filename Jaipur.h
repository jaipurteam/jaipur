#ifndef GAME_H
#define GAME_H

#include "Market.h"
#include "Deck.h"
#include "Player.h" 
#include "AllTokens.h"
#include "AllBonus.h"
#include <iostream>

class Jaipur 
{
 private:
  Market market();
  Deck deck();
  Player &p1();
  Player &p2();
  All_Tokens tokens();
  All_Bonus bonus();
  int playerTurn; //Either 0 or 1 (either can be Human or AI)
  int round, turn; // helper variables for displaying round and turn (attempt) number in main function

 public:
  Jaipur();
  ~Jaipur();

  // functions to verify gameplay state changes (to Jaipur field variables) before executing them, displaying appropriate UI separately
  bool getEndGame();
  bool getEndRound(); //return a bool if the round is done or not 
  bool getEndTurn(); //returns 1 for success, 0 for end of round, -1 for invalid move
  Player & getCurrPlayer(); //return current player to prompt for moves
  void Display();
};

#endif 
