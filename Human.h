/* Human is subclass of the player that is used to 
   define the action of human player*/


#ifndef HUMAN_H
#define HUMAN_H

#include "Player.h"
#include "Deck.h"
#include <string>

class Human: public Player{
private:
    int action;
public:
  Human(Deck &deck, std::string playerName);
  ~Human();

  virtual void setAction();
};

#endif
