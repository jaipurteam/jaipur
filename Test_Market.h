#ifndef   TEST_MARKET_H
#define   TEST_MARKET_H

#include "Market.h"

// test the default constructor
void Test_Market(Market &);
// test the conversion constructor
void Test_Market_conv(Market &);

// test getGoods
void Test_MgetGoods(Market &);

// test setGoods
void Test_MsetGoods(Market &);


#endif
