/*Test for the AllTokens class*/

#ifndef  TEST_ALLTOKENS_H
#define  TEST_ALLTOKENS_H

#include "AllTokens.h"
#include <vector>

// test for constructor: check all tokens and their sizes
void test_AllTokens(All_Tokens &);

// test for getters
void test_ATgetSize(All_Tokens &);
void test_ATgetToken(All_Tokens &);


// test for setters

#endif




