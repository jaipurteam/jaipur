#ifndef _GAME_H
#define _GAME_H

#include "Jaipur.h"

Jaipur::Jaipur(){ //construct a game
    // Initialize game variables: 
	 //deck();//initialize deck. 
    market();//initialize market. this also deletes that moved from the deck
    p1();//initialize player1. this also deletes the 5 cards from the dect that were moved to player's hand
    p2();//initialize player2. same as above
    tokens();//initialize all tokens
    bonus();//initialize all bonus
	 playerTurn = 0; // player 1 goes first 
	 
	 // Game intro display:
	 std::cout << "Welcome to Jaipur+ for Professional and Hobbyist in the Jaipur arts!\n";

	 // start game, play rounds until game over
	 while(!getEndGame()){
		  // start round, play turns until round over
		  std::cout << "Round " << round << '\n';
		  while(!getEndRound()){
					
		    // start turn, prompt player until turn validated, then execute move
		    while(!getEndTurn()){//<---validating move and executing in this method, if not then prompt for valid move
		      
		    std::cout << "Please enter valid move: ";
		    }					
		  }
		  // round has just ended:
		  giveOutSeal(); // give seal to best player
	 }
}
Jaipur::~Jaipur(){
}
// check if game finished 
// return 0 for play next round
// return 1 for game over
bool Jaipur::getEndGame(){
	if(P[0].getPoint(3) == 2 or P[1].getPoint(3) == 2)
		  return 1; // someone has won 
	 else
		  return 0; // no one has won, keep playing
}

// check if round finished
// return 0 for play next turn
// return 1 for round over
bool Jaipur::getEndRound(){
	 int c, cToken;
	 //check for empty market
	 for(c = 0; c < market.getSize(); c++){
		  if(market.getGoods(c) == 0)
				return 1; // empty card found -> market not filled -> round over
	 }
	 //check for depleted goods tokens
	 for(c = 1; c <= 6; c++){
		  if(tokens.getSize(c) == 0)
				cToken++;
	 }
	 if(cToken >= 3)
		  return 1; // three goods tokens depleted -> round over
	 return 0; // round has not ended, play next turn 
}

// whether the turn received from the user is valid
// return 0 for no valid move selected (either prev. invalid or first time prompting player)
// return 1 for valid move received and executed, prompt move from other player
bool Jaipur::getEndTurn() // input move variables as parameters
{	 // check for valid turn

}


// return a refference to the player that should make a turn
Player & Jaipur::getCurrPlayer(){
	 if(playerTurn == 0)
		  return p1;
	 else
		  return p2;
}
// update player field: point[3] == number of seals, based on the round points
void giveOutSeal(){
	 //add up points and give out seal
}

void Jaipur::Display(){
	 // //Commented the following to prevent printouts during debug:
	 // std::cout << "Printing market...\n";
         //market.printMarket();
	 // std::cout << "Printing tokens...\n";
	 //tokens.printTokens();
	 // std::cout << "Printing bonus...\n";
	 //bonus.pBonus();
}


#endif
