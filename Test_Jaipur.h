/*Test for the AllTokens class*/

#ifndef  TEST_JAIPUR_H
#define  TEST_JAIPUR_H

#include "AllTokens.h"
#include "AllBonus.h"
#include "Deck.h"
#include "Player.h"
#include "Market.h"
#include "Human.h"
#include <vector>

/* TEST_Market*/
void test_market(Market&);
void test_market32(Market&);
void test_getGoods(Market&);
void test_setGoods1(Market&);
void test_delget(Market&);

/* TEST_DECK*/
//test for default constructor
void test_Deck(Deck& );

//test for shuffle(also drawcard inside)
void test_shuffle(Deck& );


/* TEST_ALLTOKENS*/
// test for constructor: check all tokens and their sizes
void test_AllTokens(All_Tokens &);

// test for getters
void test_ATgetSize(All_Tokens &);
void test_ATgetToken(All_Tokens &);


// test for setters

/* TEST_ALLBONUS*/

//test constructor
void test_AllBonus(All_Bonus& );

// test for getters
void test_ABgetSize(All_Bonus& );
void test_ABgetBonus(All_Bonus& );

//test shuffle
void test_ABshuffle(All_Bonus& );

/*test for Player*/

void test_player_cons(Human &,std::vector<std::string> &);
void test_player_sell(std::vector<int>, All_Tokens &, All_Bonus &, Human &);
void test_player_take(Human &, Market &, Deck &); 
void test_player_trade(std::vector<int> &, std::vector<int> &, Market &, Human &);
void test_player_takeCamel(Market &, Deck &, Human &);

#endif




