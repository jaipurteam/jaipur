#include "Deck.h"
#include <cassert>
#include <iostream>

using std::cout;
using std::endl;

int main()
{
  Card Diamonds("diamonds");
  Card Gold("gold");
  Card Silver("silver");
  Card Cloth("cloth");
  Card Spice("spice");
  Card Leather("leather");
  Card Camel("camel");
 
  //test constructor 
  Deck deck;
  deck.pDeck();
  assert(deck.fCard(Diamonds)==6);
  assert(deck.fCard(Gold)==6);
  assert(deck.fCard(Silver)==6);
  assert(deck.fCard(Cloth)==8);
  assert(deck.fCard(Spice)==8);
  assert(deck.fCard(Leather)==10);
  assert(deck.fCard(Camel)==11);
  assert(deck.getsize()==55);

  //test shuffle 
  deck.shuffle();
  cout<<"----------------"<<endl;
  cout<<"After Shuffle"<<endl;
  deck.pDeck();//print out deck after shuffle

  deck.shuffle();
  cout <<"--------------"<<endl; 
  cout<<"shuffle twice"<<endl;
  deck.pDeck();//ensure the everytime the result of shuffle is different
  assert(deck.fCard(Diamonds)==6);
  assert(deck.fCard(Gold)==6);
  assert(deck.fCard(Silver)==6);
  assert(deck.fCard(Cloth)==8);
  assert(deck.fCard(Spice)==8);
  assert(deck.fCard(Leather)==10);
  assert(deck.fCard(Camel)==11);
  assert(deck.getsize()==55);

  //test draw a card
  Card *temp=new Card();
  *temp=deck.drawCard();//draw first card
  cout<<"---I am a seperate line---"<<endl;
  cout<<"the card being drawed is ";
  cout<<temp->getCard()<<endl; 
  cout<<"---I am a seperate line---"<<endl;
  cout<<"Now the deck is :"<<endl; 
  assert(deck.getsize()==54);
  deck.pDeck();

  *temp=deck.drawCard();//draw second card
  cout<<"---I am a seperate line---"<<endl;
  cout<<"the card being drawed is ";
  cout<<temp->getCard()<<endl; 
  cout<<"---I am a seperate line---"<<endl;
  cout<<"Now the deck is :"<<endl;
  assert(deck.getsize()==53);
  deck.pDeck();

  cout << "all tests have passed"<<endl;
}
