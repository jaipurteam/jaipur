/* AI is the subclass of Player*/

#ifndef AI_H
#define AI_H
#include "Player.h"

class AI: public Player
{
 public:
  void getAction();// It defines the decision of AI
};

#endif 
